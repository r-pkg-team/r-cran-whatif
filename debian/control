Source: r-cran-whatif
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-lpsolve,
               r-cran-pbmcapply
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-whatif
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-whatif.git
Homepage: https://cran.r-project.org/package=WhatIf
Rules-Requires-Root: no

Package: r-cran-whatif
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R evaluate counterfactuals
 Inferences about counterfactuals are essential for prediction,
 answering what if questions, and estimating causal effects.
 However, when the counterfactuals posed are too far from the data at
 hand, conclusions drawn from well-specified statistical analyses
 become based largely on speculation hidden in convenient modeling
 assumptions that few would be willing to defend. Unfortunately,
 standard statistical approaches assume the veracity of the model
 rather than revealing the degree of model-dependence, which makes this
 problem hard to detect. WhatIf offers easy-to-apply methods to
 evaluate counterfactuals that do not require sensitivity testing over
 specified classes of models. If an analysis fails the tests offered
 here, then it is known that substantive inferences will be sensitive to
 at least some modeling choices that are not based on empirical evidence,
 no matter what method of inference one chooses to use. WhatIf
 implements the methods for evaluating counterfactuals discussed in
 Gary King and Langche Zeng, 2006, "The Dangers of Extreme
 Counterfactuals," Political Analysis 14 (2) <DOI:10.1093/pan/mpj004>;
 and Gary King and Langche Zeng, 2007, "When Can History Be Our Guide? The
 Pitfalls of Counterfactual Inference," International Studies
 Quarterly 51 (March) <DOI:10.1111/j.1468-2478.2007.00445.x>.
